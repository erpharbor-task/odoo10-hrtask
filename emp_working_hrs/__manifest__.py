# -*- coding: utf-8 -*-
{
    'name': "hr_attendance_system",

    'summary': """
        Manage Attendance Easily""",

    'description': """
        Employee Attendance Management System
    """,

    'author': "ERP-Harbor",
    'website': "http://www.erpharbor.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'HR',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/emp_attendance_view.xml',
    ],
}
