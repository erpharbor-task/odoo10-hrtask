from odoo import models,fields,api
import datetime
from odoo import exceptions

class emp_attendance_system(models.Model):
    _name = 'hr.attendanceshow'
    employee_id = fields.Many2one('hr.employee', string='Employee Details')
    check_in = fields.Datetime(string="Check In",default=datetime.datetime.now())
    check_out = fields.Datetime(string="Check Out")
    hours = fields.Char('Working Hours',compute="find_hrs")
    today_date = fields.Char(string='Date',default=datetime.date.today())
    late_time = fields.Float(string="Late Time" )

    @api.depends('check_out')
    def find_hrs(self):
        print(self.check_in)
        print(type(self.check_in))
        if self.check_out == False:
            exceptions.ValidationError('Please Check_out to get Working Time..!')
        else:
            datetime_object1 = datetime.datetime.strptime(str(self.check_in), '%Y-%m-%d %H:%M:%S')
            datetime_object2 = datetime.datetime.strptime(str(self.check_out), '%Y-%m-%d %H:%M:%S')
             = datetime_object2 - datetime_object1
        # test = self.env['hr.attendance'].search([])
        # print(test)
        # for data in test:
        #     if data.employee_id.ids == self.test_id.ids:
        #         datetime_object1 = datetime.datetime.strptime(data.check_in, '%Y-%m-%d %H:%M:%S')
        #         datetime_object2 = datetime.datetime.strptime(data.check_out, '%Y-%m-%d %H:%M:%S')
        #         working_hrs = datetime_object2 - datetime_object1
        #         self.hours = working_hrs
        #         self.today_date = data.check_in[0:10]
        #         print(working_hrs)
        #         break

        #         get_hrs = data
        # print(get_hrs.check_in)


        # print(test,"from attendance")
        # print(self.test_id.ids,"Employee Details")

