from twilio.rest import Client
from twilio.twiml.messaging_response import MessagingResponse


def send_wp_msg(self, account_sid, auth_token, msg_body, from_number, to_number):
    client = Client(account_sid, auth_token)
    client.messages.create(
        body=msg_body,
        from_='whatsapp:'+ from_number,
        to='whatsapp:' + to_number
    )
