from odoo import api, fields, models


class ResConfigSettings(models.TransientModel):
    _inherit = 'base.config.settings'


    acc_sid = fields.Boolean("acc_sid")
    acc_token = fields.Boolean("acc_token")
    whatsapp_num_check = fields.Boolean("whatsapp_num_check")
    account_sid = fields.Char("Account Sid",related='company_id.account_sid')
    account_token = fields.Char("Account Token",related='company_id.account_token')
    whatsapp_num = fields.Char("Whatapp Number",related='company_id.whatsapp_num')
    
    