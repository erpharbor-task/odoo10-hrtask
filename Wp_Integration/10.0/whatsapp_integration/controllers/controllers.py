from odoo import http
from odoo.http import request

class WhatsappData(http.Controller):
    @http.route('/whatsapp',auth='none',csrf=False,type='http')
    def get_wp_data(self, **kw):
        if kw['Body']: 
            message_append = kw['Body']
        if kw['From']:
            mobile_num = kw['From']
            cust_num = mobile_num[9:]
            data = request.env['res.partner'].sudo().search([('mobile','=',cust_num)])
            data.sudo().message_post(message_append,message_type="comment",email_from=data.name,author_id=False)  