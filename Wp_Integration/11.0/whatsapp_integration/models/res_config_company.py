from odoo import models,fields,api

class ResCompany(models.Model):
    _inherit = 'res.company'

    account_sid = fields.Char("Account Sid")
    account_token = fields.Char("Account Token")
    whatsapp_num = fields.Char("Whatapp Number")
