from odoo import models,fields,api
from ..models import send_wp_message


class whatsappMsg(models.TransientModel):
     _name= 'whatsapp.wiz'
     _inherit = ['mail.thread']

     body_msg = fields.Text(string="Message")

     @api.multi
     def send_msg(self):
         company = self.env.user.company_id
         for partner in self.env['res.partner'].browse(self._context.get('active_ids', [])):
             if not partner.mobile:
                 continue
             send_wp_message.send_wp_msg(self,company.account_sid, company.account_token,
                                         self.body_msg, company.whatsapp_num, partner.mobile)
             partner.message_post(self.body_msg, message_type="comment")