from odoo import api, fields, models


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'


    acc_sid = fields.Boolean("acc_sid")
    acc_token = fields.Boolean("acc_token")
    whatsapp_num_check = fields.Boolean("whatsapp_num_check")
    account_sid = fields.Char("Account Sid",config_parameter='whatsapp_integration.account_sid')
    account_token = fields.Char("Account Token",config_parameter='whatsapp_integration.account_token')
    whatsapp_num = fields.Char("Whatapp Number",config_parameter='whatsapp_integration.whatsapp_num')
    
    