# -*- coding: utf-8 -*-
{
    'name': "Whatsapp Integration",

    'summary': """
        Now send Messages with Whatsapp""",

    'description': """
        with whatsapp Integation you can send and recieve messages via whatsapp
    """,

    'author': "ERP Harbor",
    'website': "http://www.erpharbor.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/11.0/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Integration',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': ['base','mail'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/res_config_settings_view.xml',
        'wizards/whatsapp_wiz_view.xml',
    ],
}