from odoo import models,fields,api
from ..models import send_wp_message


class whatsappMsg(models.TransientModel):
     _name= 'whatsapp.wiz'
     _inherit = ['mail.thread']

     body_msg = fields.Text(string="Message")

     @api.multi
     def send_msg(self):
         account_sid =self.env['ir.config_parameter'].sudo().get_param('whatsapp_integration.account_sid')
         account_token =self.env['ir.config_parameter'].sudo().get_param('whatsapp_integration.account_token')
         whatsapp_num =self.env['ir.config_parameter'].sudo().get_param('whatsapp_integration.whatsapp_num')
         
         for partner in self.env['res.partner'].browse(self._context.get('active_ids', [])):
             if not partner.mobile:
                 continue
             send_wp_message.send_wp_msg(self, account_sid, account_token,
                                         self.body_msg, whatsapp_num, partner.mobile)
             partner.message_post(body=self.body_msg,)