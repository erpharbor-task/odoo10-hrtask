from odoo import http
from odoo.http import request
import xmlrpc.client
import json


class WhatsappData(http.Controller):
    @http.route('/whatsapp', auth='none', type='http', csrf=False)
    def get_wp_data(self, **kw):
        message_append = kw['Body']
        mobile_num = kw['From']
        cust_num = mobile_num[9:]
        data = request.env['res.partner'].sudo().search([('mobile','=',cust_num)])
        data.sudo().message_post(body=message_append,email_from=data.name,author_id=False)
