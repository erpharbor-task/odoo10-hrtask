from square.client import Client
import random
import string

from odoo import api, fields, models


class AcquirerSquare(models.Model):
    _inherit = 'payment.acquirer'

    provider = fields.Selection(selection_add=[('square', 'Square')])
    square_application_id = fields.Char(string="Application ID")
    square_location_id = fields.Char(string="Location ID")
    square_access_token = fields.Char(string="Access Token")
    globvar = {}

    @api.multi
    def square_form_generate_values(self, tx_values):
        print(tx_values,"this is values!!!!!by me\n\n")
        self.ensure_one()
        square_tx_values = dict(tx_values)
        global globvar
        globvar = tx_values

        temp_square_tx_values = {
            'application_id' : self.square_application_id,
            'location_id' : self.square_location_id,
            'access_token' : self.square_access_token,
            'company': self.company_id.name,
            'ORDER_ID': tx_values['reference'],
            'CUST_ID': tx_values['reference'],
            'amount': tx_values['amount'],  # Mandatory
            'currency': tx_values['currency'].name,  # Mandatory anyway
            'currency_id': tx_values['currency'].id,  # same here
            'address_line1': tx_values.get('partner_address'),  # Any info of the partner is not mandatory
            'address_city': tx_values.get('partner_city'),
            'address_country': tx_values.get('partner_country') and tx_values.get('partner_country').name or '',
            'email': tx_values.get('partner_email'),
            'address_zip': tx_values.get('partner_zip'),
            'name': tx_values.get('partner_name'),
            'phone': tx_values.get('partner_phone'),
        }

        temp_square_tx_values['returndata'] = square_tx_values.pop('return_url', '')
        square_tx_values.update(temp_square_tx_values)
        globvar.update(temp_square_tx_values)
        print(square_tx_values,"this is values!!!\n")
        return square_tx_values

        
        # result = client.locations.list_locations()
        # print result.body
        # print "this is locations!!!"

        # mr = client.merchants.list_merchants()
        # print mr.body
        # print "this is merchant!!!!"
    
    @api.multi
    def square_get_form_action_url(self):
        print(globvar,"this is all values from globla varibale!!!!!\n")
        print(self._context,"this is context valies!!\n")
        print(self,"this is self called!!!\n")
        client = Client(
            access_token=globvar['access_token'],
            environment='sandbox',
        )
        location_id = globvar['location_id']
        sale_order = self.env['payment.transaction'].search([('reference', '=', globvar['reference'])]).sale_order_id
        if sale_order:
            body = {}
            body['idempotency_key'] = self.get_random_string(36)
            body['order'] = {}
            body['order']['order'] = {}
            body['order']['order']['location_id'] = globvar['location_id']
            body['order']['order']['reference_id'] = globvar['reference']
            body['order']['order']['customer_id'] = self.env.user.name
            body['order']['order']['line_items'] = []

            for order_line in sale_order.order_line:
                body['order']['order']['line_items'].append({})
                body['order']['order']['line_items'][0]['name'] = order_line.product_id.name
                body['order']['order']['line_items'][0]['quantity'] = str(order_line.product_uom_qty)

                if order_line.tax_id:
                    body['order']['order']['line_items'][0]['applied_taxes'] = []

                    for tax in order_line.tax_id:
                        body['order']['order']['line_items'][0]['applied_taxes'].append({})
                        body['order']['order']['line_items'][0]['applied_taxes'][0]['tax_uid'] = tax.id

                if order_line.discount:
                    body['order']['order']['line_items'][0]['applied_discounts'] = []
                    body['order']['order']['line_items'][0]['applied_discounts'].append({})
                    body['order']['order']['line_items'][0]['applied_discounts'][0]['discount_uid'] = order_line.discount

                body['order']['order']['line_items'][0]['base_price_money'] = {}
                print(float(order_line.price_subtotal),"this is price!!!\n\n\n")
                body['order']['order']['line_items'][0]['base_price_money']['amount'] = int(order_line.price_subtotal)
                body['order']['order']['line_items'][0]['base_price_money']['currency'] = sale_order.currency_id.name

            # body['order']['order']['taxes'] = [] 
            # body['order']['order']['taxes'].append({})
            # body['order']['order']['taxes'][0]['uid'] = get_random_string(50)
            # body['order']['order']['taxes'][0]['type'] = 'INCLUSIVE'
            # body['order']['order']['taxes'][0]['percentage'] = 12
            # body['order']['order']['taxes'][0]['scope'] = 'ORDER'
            # body['order']['order']['taxes'][0]['name'] = 'Total Tax'

            # body['order']['order']['discounts'] = []

            # body['order']['order']['discounts'].append({})
            # body['order']['order']['discounts'][0]['uid'] = '56ae1696-z1e3-9328-af6d-f1e04d947gd4'
            # body['order']['order']['discounts'][0]['type'] = 'FIXED_AMOUNT'
            # body['order']['order']['discounts'][0]['amount_money'] = {}
            # body['order']['order']['discounts'][0]['amount_money']['amount'] = 100
            # body['order']['order']['discounts'][0]['amount_money']['currency'] = 'USD'
            # body['order']['order']['discounts'][0]['scope'] = 'LINE_ITEM'
            # body['order']['order']['discounts'][0]['name'] = 'LINE_NAME'

            body['order']['idempotency_key'] = self.get_random_string(36)
            body['ask_for_shipping_address'] = True
            body['merchant_support_email'] = 'merchant+support@website.com'
            body['pre_populate_buyer_email'] = 'example@email.com'
            body['pre_populate_shipping_address'] = {}
            body['pre_populate_shipping_address']['address_line_1'] = globvar['billing_partner_address']
            body['pre_populate_shipping_address']['locality'] = globvar['billing_partner_city']
            body['pre_populate_shipping_address']['postal_code'] = globvar['billing_partner_zip']
            body['pre_populate_shipping_address']['country'] = globvar['billing_partner_country'].code
            body['pre_populate_shipping_address']['first_name'] = globvar['billing_partner_name']
            body['pre_populate_shipping_address']['last_name'] = globvar['billing_partner_last_name']
            # body['redirect_url'] = "https://localhost:8010/shop"
            result = client.checkout.create_checkout(location_id,body)
            print(result,"this is result!!!\n")
            if result.is_success():
                print(result.body['checkout']['checkout_page_url'],"this is url!!!\n")
                return result.body['checkout']['checkout_page_url']
            else:
                return globvar['returndata']

        else:
            return "/shop/payment/validate"
    def get_random_string(self,length):
        letters = string.ascii_lowercase + string.ascii_uppercase + string.digits
        result_str = ''.join(random.choice(letters) for i in range(length))
        return result_str
        print("Random string of length", length, "is:", result_str)
    
class ResPartnerExt(models.Model):
    _inherit = "res.partner"

    @api.multi
    def test_square_payment(self):
        client = Client(
            access_token='EAAAEHR-dCSvFMq74LBrT92DulmFQKptReDOFeXMKabED6pAzlFF8bnkwWeN2Fhj',
            environment='sandbox',
        )
        # result = client.locations.list_locations()
        # print result.body
        # print "this is locations!!!"

        # mr = client.merchants.list_merchants()
        # print mr.body
        # print "this is merchant!!!!"

        location_id = 'L9VJWW9WE988T'
        body = {}
        body['idempotency_key'] = '86ae1696-b1e3-4328-af6d-f1e04d947ad6'
        body['order'] = {}
        body['order']['order'] = {}
        body['order']['order']['location_id'] = 'L9VJWW9WE988T'
        body['order']['order']['reference_id'] = 'reference_id'
        body['order']['order']['customer_id'] = 'customer_id'
        body['order']['order']['line_items'] = []

        body['order']['order']['line_items'].append({})
        body['order']['order']['line_items'][0]['name'] = 'Printed T Shirt'
        body['order']['order']['line_items'][0]['quantity'] = '2'
        body['order']['order']['line_items'][0]['applied_taxes'] = []

        body['order']['order']['line_items'][0]['applied_taxes'].append({})
        body['order']['order']['line_items'][0]['applied_taxes'][0]['tax_uid'] = '38ze1696-z1e3-5628-af6d-f1e04d947fg3'

        body['order']['order']['line_items'][0]['applied_discounts'] = []

        body['order']['order']['line_items'][0]['applied_discounts'].append({})
        body['order']['order']['line_items'][0]['applied_discounts'][0]['discount_uid'] = '56ae1696-z1e3-9328-af6d-f1e04d947gd4'

        body['order']['order']['line_items'][0]['base_price_money'] = {}
        body['order']['order']['line_items'][0]['base_price_money']['amount'] = 1500
        body['order']['order']['line_items'][0]['base_price_money']['currency'] = 'USD'

        body['order']['order']['line_items'].append({})
        body['order']['order']['line_items'][1]['name'] = 'Slim Jeans'
        body['order']['order']['line_items'][1]['quantity'] = '1'
        body['order']['order']['line_items'][1]['base_price_money'] = {}
        body['order']['order']['line_items'][1]['base_price_money']['amount'] = 2500
        body['order']['order']['line_items'][1]['base_price_money']['currency'] = 'USD'

        body['order']['order']['line_items'].append({})
        body['order']['order']['line_items'][2]['name'] = 'Woven Sweater'
        body['order']['order']['line_items'][2]['quantity'] = '3'
        body['order']['order']['line_items'][2]['base_price_money'] = {}
        body['order']['order']['line_items'][2]['base_price_money']['amount'] = 3500
        body['order']['order']['line_items'][2]['base_price_money']['currency'] = 'USD'

        body['order']['order']['taxes'] = []

        body['order']['order']['taxes'].append({})
        body['order']['order']['taxes'][0]['uid'] = '38ze1696-z1e3-5628-af6d-f1e04d947fg3'
        body['order']['order']['taxes'][0]['type'] = 'INCLUSIVE'
        body['order']['order']['taxes'][0]['percentage'] = '7.75'
        body['order']['order']['taxes'][0]['scope'] = 'LINE_ITEM'
        body['order']['order']['taxes'][0]['name'] = 'LINE_TEXT'

        body['order']['order']['discounts'] = []

        body['order']['order']['discounts'].append({})
        body['order']['order']['discounts'][0]['uid'] = '56ae1696-z1e3-9328-af6d-f1e04d947gd4'
        body['order']['order']['discounts'][0]['type'] = 'FIXED_AMOUNT'
        body['order']['order']['discounts'][0]['amount_money'] = {}
        body['order']['order']['discounts'][0]['amount_money']['amount'] = 100
        body['order']['order']['discounts'][0]['amount_money']['currency'] = 'USD'
        body['order']['order']['discounts'][0]['scope'] = 'LINE_ITEM'
        body['order']['order']['discounts'][0]['name'] = 'LINE_NAME'

        body['order']['idempotency_key'] = '12ae1696-z1e3-4328-af6d-f1e04d947gd4'
        body['ask_for_shipping_address'] = True
        body['merchant_support_email'] = 'merchant+support@website.com'
        body['pre_populate_buyer_email'] = 'example@email.com'
        body['pre_populate_shipping_address'] = {}
        body['pre_populate_shipping_address']['address_line_1'] = '1455 Market St.'
        body['pre_populate_shipping_address']['address_line_2'] = 'Suite 600'
        body['pre_populate_shipping_address']['locality'] = 'San Francisco'
        body['pre_populate_shipping_address']['administrative_district_level_1'] = 'CA'
        body['pre_populate_shipping_address']['postal_code'] = '94103'
        body['pre_populate_shipping_address']['country'] = 'US'
        body['pre_populate_shipping_address']['first_name'] = 'Jane'
        body['pre_populate_shipping_address']['last_name'] = 'Doe'
        body['redirect_url'] = 'https://localhost:8010/web'
        

        
        result = client.checkout.create_checkout(location_id,body)
        print result,"this is result"
        if result.is_success():
            print result.body
        elif result.is_error():
            print result.errors