# -*- coding: utf-8 -*-
from odoo import http, _, fields
from odoo.http import request

class StripeController(http.Controller):
    @http.route('/set_test_demo',type='json', auth="public", website=True)
    def set_test_demo(self,**kwargs):
        result = client.checkout.create_checkout(
  location_id = "location_id0",
  body = {
    "idempotency_key": "86ae1696-b1e3-4328-af6d-f1e04d947ad6",
    "order": {
      "order": {
        "location_id": "location_id",
        "reference_id": "reference_id",
        "customer_id": "customer_id",
        "line_items": [
          {
            "name": "Printed T Shirt",
            "quantity": "2",
            "applied_taxes": [
              {
                "tax_uid": "38ze1696-z1e3-5628-af6d-f1e04d947fg3"
              }
            ],
            "applied_discounts": [
              {
                "discount_uid": "56ae1696-z1e3-9328-af6d-f1e04d947gd4"
              }
            ],
            "base_price_money": {
              "amount": 1500,
              "currency": "USD"
            }
          },
          {
            "name": "Slim Jeans",
            "quantity": "1",
            "base_price_money": {
              "amount": 2500,
              "currency": "USD"
            }
          },
          {
            "name": "Woven Sweater",
            "quantity": "3",
            "base_price_money": {
              "amount": 3500,
              "currency": "USD"
            }
          }
        ],
        "taxes": [
          {
            "uid": "38ze1696-z1e3-5628-af6d-f1e04d947fg3",
            "type": "INCLUSIVE",
            "percentage": "7.75",
            "scope": "LINE_ITEM"
          }
        ],
        "discounts": [
          {
            "uid": "56ae1696-z1e3-9328-af6d-f1e04d947gd4",
            "type": "FIXED_AMOUNT",
            "amount_money": {
              "amount": 100,
              "currency": "USD"
            },
            "scope": "LINE_ITEM"
          }
        ]
      },
      "idempotency_key": "12ae1696-z1e3-4328-af6d-f1e04d947gd4"
    },
    "ask_for_shipping_address": True,
    "merchant_support_email": "merchant+support@website.com",
    "pre_populate_buyer_email": "example@email.com",
    "pre_populate_shipping_address": {
      "address_line_1": "1455 Market St.",
      "address_line_2": "Suite 600",
      "locality": "San Francisco",
      "administrative_district_level_1": "CA",
      "postal_code": "94103",
      "country": "US",
      "first_name": "Jane",
      "last_name": "Doe"
    },
    "redirect_url": "https://merchant.website.com/order-confirm",
    "additional_recipients": [
      {
        "location_id": "057P5VYJ4A5X1",
        "description": "Application fees",
        "amount_money": {
          "amount": 60,
          "currency": "USD"
        }
      }
    ]
  }
)
        return request.env['ir.ui.view'].render_template('ehcs_square_payment_integration.square_payment_form', {})
