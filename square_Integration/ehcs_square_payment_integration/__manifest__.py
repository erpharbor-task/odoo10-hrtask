# -*- coding: utf-8 -*-
{
    'name': "ehcs_square_payment_integration",
    'summary': """Integration of Square Payment Acquirer with Odoo""",
    'description': """Integration of Square Payment Acquirer with Odoo""",
    'author': "ERP Harbor consulting services",
    'website': "www.erpharbor.com",
    'category': 'API',
    'version': '10.0.0.0',
    'depends': [
        'base',
        'payment',
        'website_sale',
    ],
    'data': [
        'views/payment_square_templates.xml',
        'views/payment_view.xml',
        'data/payment_square.xml',
        # 'security/ir.model.access.csv',
    ],
    # 'qweb': [
    #     "static/src/xml/square_payment_from.xml",
    # ],
    # "external_dependencies": {"python" : ["squareup"]}
}