# -*- coding: utf-8 -*-
{
    'name': "HotelManagement",

    'summary': """
        Here You can easily Manage Your all Hotel activity..!""",

    'description': """
        Now Manage Hotel Easily just on few click..!
    """,

    'author': "Husain Rangwala",
    'website': "http://www.Learnodoo.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/11.0/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Social',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': ['base','mail'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'wizards/wiz_hotel_view.xml',
        'wizards/wiz_cust_view.xml',
        'wizards/wiz_staff_view.xml',
        'wizards/wiz_staff_wh_view.xml',
        'wizards/whatsapp_wiz_view.xml',
        'views/hotel_view.xml',
        'views/customer_view.xml',
        'views/staff_view.xml',
        'views/room_view.xml',
        'views/food_view.xml',
        'views/res_partner_view.xml',
        'views/csinherit_view.xml',
        'views/res_config_settings_views.xml',
        'reports/wiz_hotel_report_pdf.xml',
        'reports/wiz_hotel_report.xml',
        'reports/staff_template_inherit.xml',
    ]
}