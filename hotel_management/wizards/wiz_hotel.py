from odoo import models,fields,api

class wix_hotel(models.TransientModel):
    _name = "wizhotel.hotel"

    total_cust = fields.Integer(string="Total Customer",compute='count_cust')
    per_cust_charge = fields.Float(string="Per Customer Charge",default=500)

    @api.depends('per_cust_charge')
    def count_cust(self):
        x = self.env['hotel.hotel'].browse(self.env.context.get('active_id'))
        z = x.total_cust
        # print("hello welcome to @@@@@@@@@@@@@@@@@@@@@@@@@@@@@",x)
        # print("total cust@@@@@@@@@@@",z)
        self.total_cust = z

    @api.multi
    def count_turnover(self):
        # hotel = self.env['hotel.hotel'].browse(self.env.context.get('active_id'))
        # total =self.total_cust * self.per_cust_charge
        # hotel.write({'todays_total_turnover':total})
        data={
            'ids':self.ids,
            'model':self._name,
            'form':{
                'total_cust' : self.total_cust,
                'per_cust_charge': self.per_cust_charge
            }
        }
        return self.env.ref('hotel_management.hotel_report').report_action(self, data=data)

class wiz_hotel_report(models.AbstractModel):
    """Abstract Model for report template.
    for `_name` model, please use `report.` as prefix then add `module_name.report_name`.
    """

    _name = 'report.hotel_management.hotel'

    @api.model
    def get_report_values(self, docids, data=None):
        total_cust = data['form']['total_cust']
        per_cust_charge = data['form']['per_cust_charge']
        print("@@@@@@@@@@@@@@@@",total_cust,per_cust_charge)
        total = total_cust * per_cust_charge

        docs = []
        hotel = self.env['hotel.hotel'].browse(self.env.context.get('active_id'))
        # total = self.total_cust * self.per_cust_charge
        hotel.write({'todays_total_turnover':total})

        docs.append({
                'name': hotel.name,
                'address': hotel.address
        })
        return {
            'doc_ids': data['ids'],
            'doc_model': data['model'],
            'total_cust': total_cust,
            'per_cust_charge': per_cust_charge,
            'docs': docs,
        }