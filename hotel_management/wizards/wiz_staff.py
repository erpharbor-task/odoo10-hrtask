from odoo import models,fields,api

class wiz_cust(models.TransientModel):
    _name = "wizstaff.hotel"

    total_working_day = fields.Integer(string="Working Days")
    total_working_Hours = fields.Float(string="Working Hours")