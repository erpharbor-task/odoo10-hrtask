from odoo import models,fields,api
from twilio.rest import Client
from twilio.twiml.messaging_response import MessagingResponse

class whatsappMsg(models.TransientModel):
     _name= 'whatsapp.wiz'
     _inherit = ['mail.thread']

     body_msg = fields.Text(string="Message")

     @api.multi
     def send_msg(self):
         data = self.env['customer.hotel'].browse(self.env.context.get('active_ids'))
         static_data = self.env['res.company'].search([])
        #  for data in static_data:
        #      print(data)
        #  print("@@@@@@",data)
         account_sid = static_data.account_sid
         auth_token = static_data.account_token
         for send_msg in data:
            client = Client(account_sid, auth_token)
            message_send = self.body_msg
            message = client.messages.create(
                body=message_send,
                from_=static_data.whatsapp_num,
                to='whatsapp:'+ send_msg.mobile_no
            )
            for data1 in send_msg:
                 data1.message_post(message_send,message_type="comment",email_from=data1.hoteldetails_id.name,author_id=False)