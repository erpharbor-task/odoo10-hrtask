from odoo import models,fields,api

class wix_staff_wh(models.TransientModel):
    _name = "wizstaffwh.hotel"

    working_hrs = fields.Char(string='working Hrs',compute='find_hrs')
    salary_per_hour = fields.Float(string='Salary Per Hours',default=100)

    @api.depends('salary_per_hour')
    def find_hrs(self):
        test = self.env['staff.hotel'].browse(self.env.context.get('active_id'))
        # print(test)
        # print(test)
        # print(test.staff_details_id.name)
        staff_tm = self.env['staff_timings.hotel'].search([('staff_details_id.name','=',test.name)])
        # print(staff_tm)
        total_time = 0
        for time in staff_tm:
            total_time += float(time.shift_time)
            # print(time.shift_time)
        # self.write({'working_hrs':total_time})
        self.working_hrs = total_time
    @api.multi
    def count_salary(self):
        rs = self.env['staff.hotel'].browse(self.env.context.get('active_id'))
        salary = float(self.working_hrs) * float(self.salary_per_hour)
        rs.write({'salary':salary})