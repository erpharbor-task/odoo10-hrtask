from odoo import models,fields, api
from odoo.exceptions import UserError,ValidationError
import datetime

class room(models.Model):
    _name = 'room.hotel'
    _sql_constraints = [('check_age', 'UNIQUE(room_no)', "Room Already Created..!")]

    room_no = fields.Selection([('1','1'),('2','2'),('3','3'),('4','4'),('5','5'),('6','6')],string="RooM NO:")
    room_type = fields.Selection([('single','Single'),('couple','Couple'),('family','Family')],string="Room Type:",required=True)
    state = fields.Selection([('clean', 'Clean'),
                              ('dirty', 'Dirty'),('cleaning required','Cleaning Required')], 'State', default='clean')
    current_status = fields.Selection([('a','Assigned'),('na','Not Assigned')],string="Current Status",readonly=True,default='na')
    check_in_time = fields.Datetime(string="Check in Time", readonly=True)
    check_out_time = fields.Datetime(string="Check out Time", readonly=True)
    avl = fields.Integer(string="Availibility",readonly=True)
    cust_id = fields.Many2one('customer.hotel',string="Customer Details")
    bill_amount = fields.Integer(string='Bill Amount:',readonly=True)
    hotel_details_id = fields.Many2one('hotel.hotel',string='Hotel Details')
    stay_time = fields.Char(string="Stay Time:")
    bill_status = fields.Selection([('paid','Paid'),('unpaid','Unpaid')],string="Bill_Status",default="unpaid")
    temp_avl = fields.Integer()





    @api.multi
    def room_checkin(self):
        self.write({'state':'clean','check_in_time':datetime.datetime.now()})
        for status in self:
            if status.current_status == 'a':
                raise UserError("Room is already assigned")
        self.write({'current_status': 'a'})
    @api.multi
    def room_checkout(self):
        self.write({'state':'cleaning required','check_out_time': datetime.datetime.now()})
        print(self.check_in_time)
        for status in self:
            if status.check_in_time == False:
                raise UserError("You can not check-out without Assigning the room..!")
        self.write({'current_status': 'na'})
        x = datetime.datetime.strptime(self.check_in_time,'%Y-%m-%d %H:%M:%S')
        y = datetime.datetime.strptime(self.check_out_time, '%Y-%m-%d %H:%M:%S')
        bill_amt = 0
        z = y-x
        print(z)
        t_days = z.days
        t_seconds = z.seconds
        if t_days > 0:
            bill_amt += (1500*t_days)
        if t_seconds > 0:
            t_hrs = (t_seconds / 3600)
            bill_amt += (100*t_hrs)
        self.write({'stay_time': z})
        self.write({'bill_amount':bill_amt})


        """print(self.check_in_time,"@@@@@DATE")
        print(type(self.check_in_time),"@@@@DATE")
        print(x,"######converted")
        print(type(x),"#####convert")
"""


    @api.multi
    def clean_req(self):
        self.write({'state':'cleaning required'})

    @api.multi
    def clean_done(self):
        self.write({'state':'clean'})

    @api.multi
    def chk_avl(self):
        x = self.search([('current_status','=','na')])
        z = len(x)
        y = self.search([])
        for vals in y:
            vals.write({'avl':z,'temp_avl':z})
    @api.multi
    def clr_data(self):
        if self.bill_status == "paid":
            self.write({'check_in_time':False,'check_out_time':False,'bill_amount':0,'cust_id':False,'hotel_details.id':False,'bill_status':'unpaid'})
        else:
            raise UserError("You can Not Clear room without without Bill Payment..!")
    @api.multi
    def bill_staatus(self):
        if self.bill_status == 'paid':
            raise UserError("Bill Already Paid..!")
        self.write({'bill_status':'paid'})



    @api.constrains('temp_avl')
    def chk_availbility(self):
        if self.temp_avl <= 0:
            raise ValidationError("No Room Available..!")

    @api.multi
    def testing(self):
        print("Hello,This is room class")
        x = 6
        return x
"""     x = {}
        x = self.search([('current_status', '=', 'na')])
        print(x,"@@@@@@@@@@@@@@@@@@@")
        print(type(x))
        avlb = len(x)
        sing_room_avl = len(room.list_single_avl)
        y = self.search([])
        for valss in y:
            valss.write({'sing_avl': sing_room_avl})
"""\

