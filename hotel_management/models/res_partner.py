from odoo import models,fields,api

class res_partner(models.Model):

    # _name = "test.test"
    # _inherits = {
    #     'res.partner': 'testing',
    # }
    _inherit="res.partner"
    # testing = fields.Many2one('res.partner',string="Testing")
    edu_qua = fields.Selection([('10th','10th'),('12th','12th'),('gd','Graduate'),('pd','Post Graduate')],string="Education Details:")
    is_cuts = fields.Boolean(string="Is Customer:")
    is_staff = fields.Boolean(string="Is Staff:")

    # @classmethod
    # def get_module_info():
    #     x = super(res_partner).get_module_info()
    #     print(x,"@@@@@@@@@@@@@@@@@@@@@@@@@@@")
    #     return x