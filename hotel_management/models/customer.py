from odoo import models,fields,api
import datetime
import random

class customer(models.Model):
    _name = 'customer.hotel'
    _inherit = ['mail.thread']
    # _sql_constraints = [
    #     ('Check age',
    #      'CHECK(age > 18)',
    #      "You Are not eligible for room..!"),
    # ]
    name = fields.Char(string="Name")
    image = fields.Binary(string="Image")
    age = fields.Integer(string="Age")
    gender =fields.Selection([('m','Male'),('f','Female')],string="Gender")
    idproof = fields.Selection([('Ac','Aadhar Card'),('Ec','Election Card'),('Pc','Pan Card')],string="Id Proof")
    feedback = fields.Html(string="Feedback")
    hoteldetails_id = fields.Many2one('hotel.hotel',string='Hotel Details')
    staff_details = fields.Many2many('staff.hotel', string="Staff")
    #_rec_name = 'age'
    red_clr = fields.Boolean(string="Red")
    blue_clr = fields.Boolean(string="Blue")
    food_details = fields.One2many('food.hotel','cust_details',string="Food Items:")
    total_bill_amount = fields.Float(string="Food Bill:")
    color = fields.Integer(string="color")
    cust_pri = fields.Selection([('0','0'),('1','1')],string="Priorities")
    # active = fields.Boolean(string="Active")
    sequence = fields.Integer('Sequence', default=1, help="The first in the sequence is the default one.")
    total_bills = fields.Float(string="Total Bill")
    mobile_no = fields.Char("Mobile")
    email = fields.Char("Email")




    @api.model
    def create_details(self,vals):
        list_name = ["kishan","Mehul","sharad","Harsh","dev"]
        list_age = [23,21,34,25,22]
        list_feedback = ["Excellent","Good","Bad","peacefull"]
        list_id = ['Ac','Ec','Pc']
        name_select = random.choice(list_name)
        age_select = random.choice(list_age)
        feedback_select = random.choice(list_feedback)
        id_select = random.choice(list_id)
        self.create({'name': name_select, 'age': age_select, 'idproof': id_select, 'feedback' : feedback_select })
        x=fields.Datetime.from_string(fields.Datetime.now())
        y=fields.Datetime.to_string(datetime.datetime.now())
        print(type(x)) #datetime
        print(type(y)) #string
        return {
            'name' : 'Customer',
            'res_model': 'customer.hotel',
            'view_type': 'form',
            'view_mode': 'tree,form',
            #'domain': [('id','=',self.ids)],
            'type': 'ir.actions.act_window'
        }

    @api.multi
    def modify_details(self, vals):
        list_name = ["kishan", "Mehul", "sharad", "Harsh", "dev"]
        list_age = [23, 21, 34, 25, 22]
        list_feedback = ["Excellent", "Good", "Bad", "peacefull"]
        list_id = ['Ac', 'Ec', 'Pc']
        name_select = random.choice(list_name)
        age_select = random.choice(list_age)
        feedback_select = random.choice(list_feedback)
        id_select = random.choice(list_id)
        self.write({'name': name_select, 'age': age_select, 'idproof': id_select, 'feedback': feedback_select})

    @api.multi
    def delete_details(self):
        self.unlink()

    @api.multi
    def read_details(self):
        x = self.read(['name','age','idproof'])
        print(x)
        return {
            'name': 'customer Details',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'customer.hotel',
            'domain': [('id', 'in', self.ids)],
            'type': 'ir.actions.act_window',
            #            'target': 'new',
        }
    @api.multi
    def copy_details(self):
        self.copy()


    @api.onchange('food_details')
    def bill_amt(self):
        sum = 0
        for data in self:
            for subdata in data.food_details:
                    x = subdata.total
                    sum = sum + x
        self.total_bill_amount = sum

    @api.multi
    def total_today_bill(self):
        x = self.search([('total_bill_amount','>',0)])
        for i in x:
            print(i.name,"$$$$$$$")
