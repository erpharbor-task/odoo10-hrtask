from odoo import models,fields, api
import random
from odoo.exceptions import UserError
from twilio.rest import Client
from twilio.twiml.messaging_response import MessagingResponse

class hotel(models.Model):
    _name = 'hotel.hotel'
    _inherit = ['mail.thread']

    name = fields.Char(string="Name", required=True)
    address = fields.Text(string="Address")
    rating = fields.Integer(string="Rating",required=True)
    custdetails_ids = fields.One2many('customer.hotel','hoteldetails_id',string="Customer")
    todays_cust = fields.Integer(string="Todays Customer")
    staff_details = fields.One2many('staff.hotel','hotel_details_id',string="Staff Details")
    total_cust = fields.Integer(string="Total Customer:",compute="count_cust")
    todays_total_turnover = fields.Float(string="Todays Turnover")

    @api.multi
    def show_data(self):
        print(self)
        print(self.name)
        print(self.address)
        print(self.rating)
        print(self.custdetails_ids)
        for cust in self.custdetails_ids:
            print(cust.name)
        return {
            'name': 'Student Details',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'customer.hotel',
            'domain': [('id', 'in', self.custdetails_ids.ids)],
            'type': 'ir.actions.act_window',
            #            'target': 'new',
        }


    @api.model
    def create(self, vals):
        if vals['todays_cust'] == None or vals['todays_cust'] == 0:
            num = random.randint(1,100)
            vals.update({'todays_cust': num})
        test = super(hotel, self).create(vals)
        print("testing is going on\n\n",self, vals, test )
        print(test.ids)
        print(test.name)
        return test

    @api.multi
    def write(self, valse):
        print("Hello odoo users..............############################")
        print(valse)
        if self.rating == None:
            valse.update({'rating': '5'})
        x = super(hotel, self).write(valse)
        return x

    @api.multi
    def unlink(self):
        # print(self)
        x = self.browse(self.ids)
        print(x)
        if self.rating == 5:
            raise UserError("Hotel with 5 star can not be delete")
        test = super(hotel, self).unlink()
        print("####################",test,"###################")
        if test == True:
            print("---------------------succesfully deleted-----------------")

    @api.multi
    def copy(self,default=None):
        if default is None:
            default = {}
        default.update({'name' : self.name+" Duplicate",'address':self.address+' Duplicate'})
        return super(hotel, self).copy(default)

    """"@api.multi
    def read(self,vals):
        print("-------------hello read the itmes-------------")
        a = super(hotel, self).read(vals)
        print(a)
        return a
"""

    @api.multi
    def name_get(self):
        super(hotel, self).name_get()
        list_data = []
        str1 = " "
        for values in self:
            str1 = values.name+ "[" + values.address +"]"
            list_data.append((values.id, str1))

        return list_data

    @api.multi
    def count_cust(self):
        x=self.custdetails_ids.ids
        self.total_cust = len(x)

    @api.multi
    def update_staff(self):
        self.write({'staff_details':[(0,self.ids,{'name':'Krunal','shifttime':'day'})]})
        return True

    @api.multi
    def get_customers(self):
        cust = self.mapped('custdetails_ids')
#         print(cust)
        action = self.env.ref('hotel_management.action_customer').read()[0]
#         print(action)
        if len(cust) == 1:
            action['views'] = [(self.env.ref('hotel_management.view_customer_form').id, 'form')]
            action['res_id'] = cust.ids[0]
        elif len(cust) > 1:
            action['domain'] = [('id', 'in', cust.ids)]
        else:
             action = {'type': 'ir.actions.act_window_close'}
        return action

