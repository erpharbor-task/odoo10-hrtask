from odoo import models,fields, api

class food(models.Model):
    _name = "food.hotel"

    item_name = fields.Char(string="Food Item Name:")
    item_type = fields.Selection([('dessert','Dessert'),('chinese','Chinese'),('punjabi','Punjabi'),('non-veg','Non-veg'),('veg','Veg'),('bevrages','Bevreges')],string="Food Item Type:")
    item_price = fields.Integer(string="Amount:")
    item_quantity = fields.Integer(string="Quantity:")
    uom = fields.Selection([('kg','KG'),('gm','GM'),('nos','NOS'),('pcs','PCS'),('ltr','LTR')],string="UOM")
    item_tax_pr = fields.Selection([('5','5%'),('12','12%'),('18','18%'),('27','27%')],string="Tax Amount(%)")
    item_tax = fields.Float(string="Tax Amount(RS)")
    total = fields.Float(string="Total:",compute="bill_count")
    cust_details = fields.Many2one('customer.hotel',string="Customer Details:")
    food_img = fields.Binary(string="Image")
    color = fields.Integer(string="color")

    @api.depends('item_quantity','item_price','item_tax')
    def bill_count(self):
        for items in self:
            #print(self)
            total = (items.item_quantity * items.item_price) + items.item_tax
            items.total = total


    @api.onchange('item_tax_pr')
    def tx_count(self):
        tx_pr = int(self.item_tax_pr)
        self.item_tax = ((self.item_quantity * self.item_price) * tx_pr) / 100

    """@api.onchange('item_tax')
       def bill_count(self):
           for items in self:
               print(self)
               total = (items.item_quantity * items.item_price) + items.item_tax
               items.total = total
   """