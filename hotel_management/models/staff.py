from odoo import models,fields,api
import datetime
import pytz
from odoo.exceptions import UserError
import time
class staff(models.Model):
    _name = 'staff.hotel'

    name = fields.Char(string="Name", required=True)
    salary = fields.Integer(string="Salary")
    shifttime = fields.Selection([('day','Day'),('night','Night')],string="Shift_timings")
    timing = fields.Selection([('d','9:00AM To 9:00PM'),('n','9:00PM TO 9:00AM')],String='Timing')
    customer_details = fields.Many2many('customer.hotel', string="Customer")
    hotel_details_id = fields.Many2one('hotel.hotel', string='Hotel Details')
    daily_working_hours = fields.Char(string="Session Time:", readonly=True)
    current_status = fields.Selection([('ci','you are Checked-IN'),('co','You are Check-OUT')],string="Current Status",readonly=True)
    check_in_time = fields.Char(String="Checked_IN")
    check_out_time = fields.Char(string="Checked_OUT")
    id_check = fields.Integer(string="ID")
    todays_payment = fields.Integer(string="Todays Payment: ",compute="count_salary")
    timing_details_ids = fields.One2many('staff_timings.hotel','staff_details_id',string="Staff timing details")






    @api.onchange('salary')
    def show_msg1(self):
        return {'warning': {'title': 'Salary Details', 'message': 'Slary sholud be less than 15000..!'}}

    @api.onchange('shifttime')
    def change(self):
        if self.shifttime == 'day':
            self.timing = 'd'
        if self.shifttime == 'night':
            self.timing = 'n'
        return {'warning':{'title':'Changing shift', 'message': 'Changing shift will Also change Your Working Time..!'}}



class staff_timings(models.Model):

    _name = 'staff_timings.hotel'

    check_in_time = fields.Char(string='Check_in_time')
    check_out_time = fields.Char(string='Check_out_time')
    staff_details_id = fields.Many2one('staff.hotel',string="Staff_details")
    id_check = fields.Integer(string="ID")
    shift_time = fields.Char(string="Shift Time Hrs")
    @api.multi
    def check_in_new(self,vals):
        if self.id_check == 0:
            tt = pytz.timezone('Asia/Kolkata')
            check_in = datetime.datetime.now(tt)
            self.check_in_time = check_in
            self.id_check = 1
        else:
            raise UserError("You already checked IN..!")

    @api.multi
    def check_out_new(self, vals):
        if self.id_check == 1:
            tt = pytz.timezone('Asia/Kolkata')
            check_out = datetime.datetime.now(tt)
            self.check_out_time = check_out
            datetime_object1 = datetime.datetime.strptime(self.check_out_time[0:26], '%Y-%m-%d %H:%M:%S.%f')
            datetime_object2 = datetime.datetime.strptime(self.check_in_time[0:26], '%Y-%m-%d %H:%M:%S.%f')
            print(datetime_object2)
            print(type(datetime_object2))
            self.shift_time = ((datetime_object1 - datetime_object2).total_seconds())/3600

            self.id_check = 0
        else:
            raise UserError("You already Already checked OUT..!")

