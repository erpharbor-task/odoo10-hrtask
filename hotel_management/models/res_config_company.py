from odoo import models,fields,api

class ResCompany(models.Model):
    _inherit = 'res.company'

    account_sid = fields.Char("account sid",default='AC655658ee05932061932e9acba29f4d1d')
    account_token = fields.Char("account token",default='47cd220371ae62788d98d89414977ede')
    whatsapp_num = fields.Char("Whatapp Number",default='whatsapp:+14155238886')
