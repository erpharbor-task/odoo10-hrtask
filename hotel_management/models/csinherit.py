from odoo import models,fields,api


class csinherit(models.Model):

    _name = "custstaff.hotel"
    customer = fields.Many2one('customer.hotel',string="customer",delegate=True)
    staff = fields.Many2one('staff.hotel', string="staff",delegate=True)
