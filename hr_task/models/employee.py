from odoo import models, fields, api


class Employee(models.Model):
    _inherit = 'hr.employee'

    age = fields.Integer("Age")
    firstname = fields.Char('First Name', required=True)
    middlename = fields.Char('Middle Name', required=True)
    lastname = fields.Char('Last Name')
    pancard = fields.Char('Pan Number')
    aadhar = fields.Char('Aadhar Number')
    attachment_ids = fields.One2many('ir.attachment', 'res_id', auto_join=True,
                                     domain=[('res_model', '=', 'hr.employee')])

    @api.model
    def create(self, vals):
        if vals['lastname']:
            vals.update({'name': vals['firstname'] + ' ' + vals['middlename'] + ' ' + vals['lastname']})
        else:
            vals.update({'name': vals['firstname'] + ' ' + vals['middlename']})
        return super(Employee, self).create(vals)

    # @api.model
    # def name_get(self):
    #     result = []
    #     for d in self:
    #         if d.firstname == False:
    #             d.firstname = 'a'
    #         if d.lastname == False:
    #             d.LastName = " c"
    #         if d.middlename == False:
    #             d.middlename = 'ss'
    #         fullname = d.firstname + ' ' + d.middlename + ' ' + d.lastname
    #         result.append((d.id, fullname))
    #     return result

    @api.onchange('firstname')
    def ssga(self):
        a = self.env['resource.resource'].search([])
        for i in a:
            print(i.name)
