from odoo import models , fields, api

class status(models.Model):
	_inherit = 'hr.employee'

	status=fields.Selection([('trainee','Trainee'),('joining','Joining'),('terminated','Terminated'),('notice','Notice'),('resigned','Resigned')],'Status',default='trainee')
	family_mambers=fields.One2many('employee.family','employee','Family Mambers')

	# @api.multi
	# def fil(self):
	# 	for j in self:
	# 		if self.status==False:
	# 			self.status='trainee'

	def joining_to(self):
		self.status='joining'

	def terminated_to(self):
		self.status='terminated'

	def notice_to(self):
		self.status='notice'

	def resigned_to(self):
		self.status='resigned'

	def re_join(self):
		self.status='joining'