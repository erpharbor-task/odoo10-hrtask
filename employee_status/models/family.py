from odoo import models, fields, api
from datetime import datetime, date


class Family(models.Model):
    _name = 'employee.family'

    name = fields.Char('Name', required=1)
    relation = fields.Selection([('father', 'Father'), ('mother', 'Mother'), ('brother', 'Brother'), (
        'sister', 'Sister'), ('wife', 'Wife'), ('childrean', 'Childrean')], 'Relation')
    age = fields.Integer('Age', compute="change")
    birthdate = fields.Date('Birthdate')
    employee = fields.Many2one('hr.employee', 'Employee')

    @api.depends('birthdate')
    def change(self):
        for i in self:
            if i.birthdate:
                p = datetime.today()
                x = datetime.strptime(i.birthdate, '%Y-%m-%d')
                i.age = (p.year-x.year)
                if p.month > x.month:
					i.age = i.age-1
