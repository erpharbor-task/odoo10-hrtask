# -*- coding: utf-8 -*-
{
    'name': "ehcs_inventory_adjustment_reverse",
    'summary': """This module provide functionality to cancel and reverse validated inventory adjustment""",
    'description': """This module provide functionality to cancel and reverse validated inventory adjustment""",
    'author': "ERP Harbor consulting services",
    'website': "www.erpharbor.com",
    'category': 'Inventory',
    'version': '13.0.0.0',
    'depends': ['base','stock'],
    'data': [
        'security/inventory_cancel.xml',
        'views/stock_inventory_view.xml',
    ],
}