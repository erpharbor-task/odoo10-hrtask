from odoo import api, fields, models
from odoo.tools.float_utils import float_round, float_is_zero

class StockInventory(models.Model):
    _inherit = "stock.move"

    def action_cancel_moves(self):
        for move in self:
            # delete moves lines if moves is in cancel state.
            if move.state == 'cancel':
                move_lines = self.env['stock.move'].mapped('move_line_ids')
                move_lines.unlink()
            if move.move_dest_ids or move.move_orig_ids:
                if move.propagate:
                    for linked_move in move.move_dest_ids.mapped('move_orig_ids'):
                        if linked_move == move:
                            continue
                        if linked_move.state == 'cancel':
                            linked_move.move_dest_ids.action_cancel_moves()
                else:
                    for lined_move in move.move_dest_ids.mapped('move_orig_ids'):
                        if linked_move == move:
                            continue
                        if linked_move.state in ('done', 'cancel'):
                            linked_move.move_dest_ids.write({'procure_method': 'make_to_stock','move_orig_ids': [(3, lined_move.id, 0)]})
        self.write({'state': 'cancel', 'move_orig_ids': [(5, 0, 0)]})
        return True

class StockMoveLineExtended(models.Model):
    _inherit = 'stock.move.line'
    
    def unlink(self):
        precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
        for move_line in self:
            if move_line.product_id.type == 'product' and not move_line.location_id.should_bypass_reservation() and not float_is_zero(move_line.product_qty, precision_digits=precision):
                quant = self.env['stock.quant']._update_reserved_quantity(move_line.product_id, move_line.location_id, -move_line.product_qty, lot_id=move_line.lot_id,package_id=move_line.package_id, owner_id=move_line.owner_id, strict=True)
                return quant
        return True