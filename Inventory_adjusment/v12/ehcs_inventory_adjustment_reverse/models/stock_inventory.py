from odoo import api, fields, models

class StockInventory(models.Model):
    _inherit = "stock.inventory"

    @api.multi
    def action_cancel_inv(self):
        moves = self.mapped("move_ids")
        moves.action_cancel_moves()
        self.write({'line_ids': [(5,)],'state': 'cancel'})

        #If product inventory set as a lot or serial tracking then we need to delete it 
        if self.filter == 'lot':
            for move in self.move_ids:
                source_quants = self.env['stock.quant'].sudo().search([('product_id','=',move.product_id.id),('lot_id','=',self.lot_id.id),('location_id','=',move.location_id.id)])
                destination_quants = self.env['stock.quant'].sudo().search([('product_id','=',move.product_id.id),('lot_id','=',self.lot_id.id),('location_id','=',move.location_dest_id.id)])

                #For source location we need to check for lots and also for serial tracking
                if source_quants:
                    if source_quants[0].product_id.tracking == 'lot':
                        source_quants[0].quantity += move.product_uom_qty
                    else:
                        if source_quants[0].lot_id.id == self.lot_id.id:
                            source_quants[0].quantity = move.quantity_done
                
                #For destination location we need to check for lot and serial tracking
                if destination_quants:
                    if destination_quants[0].reserved_quantity:
                        destination_quants[0].quantity -= move.product_uom_qty
                        destination_quants[0].quantity -= destination_quants[0].reserved_quantity
                    else:
                        destination_quants[0].quantity -= move.product_uom_qty
        
        #if product tracking set without serial or lot tracking
        else:
            for move in self.move_ids:
                source_quants = self.env['stock.quant'].sudo().search([('product_id','=',move.product_id.id),('location_id','=',move.location_id.id)])
                destination_quants = self.env['stock.quant'].sudo().search([('product_id','=',move.product_id.id),('location_id','=',move.location_dest_id.id)])
                
                if source_quants[0]:
                    source_quants[0].quantity += move.product_uom_qty
                
                if destination_quants[0]:
                    #if quant is reserved then we need to minus reserved quantity
                    if destination_quants[0].reserved_quantity:
                        destination_quants[0].quantity -= move.product_uom_qty
                        destination_quants[0].quantity -= destination_quants[0].reserved_quantity
                    else:
                        destination_quants[0].quantity -= move.product_uom_qty

    @api.multi
    def action_reset_to_draft(self):
        self.write({'state': 'draft','line_ids': [(5,)]})
                        
